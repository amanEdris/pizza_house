package agent 

type AgentInterface interface {
	ListenMessage()
	SetOutChannel(chan Message)
	GetMessage(Message)
	SendMessage(Message) 
	ReadMessage() Message
	SetId(int)
	GetId() int
}


type Agent struct {
	id        int
	Recieve     chan Message
	send    chan Message
}

func (a *Agent) SetId(id int){
	 a.id = id 
}

// Name receives a copy of Foo since it doesn't need to modify it.
func (a *Agent) GetId() int {
    return a.id
}

func (a *Agent) SetOutChannel(ch chan Message) {
	a.send = ch
}


func  NewAgent(id int, cs chan Message) *Agent {
	return &Agent{
           id: id, 
	   	   Recieve: cs,
      }
}


//accept message from agent manager
func (a *Agent) GetMessage(m Message) {
	a.Recieve <- m
}

//recive any input message
func (a *Agent) ReadMessage() Message {
	return <-a.Recieve
}

//send message to other agents
func (a *Agent) SendMessage(m Message) {
	a.send <- m
}




