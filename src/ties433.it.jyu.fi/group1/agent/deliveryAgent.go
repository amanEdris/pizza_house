package agent 

import (
    "fmt"
	"time"
)

type DeliveryAgent struct {
	*Agent
}

func  NewDeliveryAgent(id int,cs chan Message) *DeliveryAgent {
	base :=  NewAgent(id,cs)
	return &DeliveryAgent{
           base,
      }
}




func (a DeliveryAgent) ListenMessage() {
    // wait until recive message 
	ticker := time.NewTicker(15 * time.Second )
    
	for {
		
		select {
				case <-ticker.C:
					fmt.Printf("Delivery waitin ..... %d \n",a.GetId())
				case msg := <- a.Recieve:			   
		   		    if(a.GetId() == msg.Header.Target){
						//do somtethin
						
						//construct message
		   		    	hdr := MessageHeader{a.GetId(), msg.Header.Target}
						a.SendMessage(Message{hdr, MessageContent{"OrderProcessed.."}})
						
		   		    }
					
					  
				   

			   }
		
}
}