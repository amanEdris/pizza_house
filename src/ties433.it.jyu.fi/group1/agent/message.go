package agent

type MessageHeader struct {
	Sender, Target int
}

type MessageContent struct {
	Data string
}

type Message struct {
	Header  MessageHeader
	Content MessageContent
}

func (m Message) CreateReply(c MessageContent) Message {
	hdr := MessageHeader{m.Header.Sender, m.Header.Target}
	return Message{hdr, c}
}
