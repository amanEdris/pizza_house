package agent 

import (
	"time"
	"fmt"
)

type OrderAgent struct {
	*Agent
}

func  NewOrderAgent(id int,cs chan Message) *OrderAgent {
  	base :=  NewAgent(id,cs)
  	return &OrderAgent{
             base,
        }
}


func (a OrderAgent) ListenMessage() {
    // wait until recive message 
	ticker := time.NewTicker(15 * time.Second )
    
	for {
		
		select {
				case <-ticker.C:
					fmt.Printf("order waitin ..... %d \n",a.GetId())
				case msg := <- a.Recieve:			   
		   		    if(a.GetId() == msg.Header.Target){
						//do somtethin
						
						//construct message
		   		    	hdr := MessageHeader{a.GetId(), msg.Header.Target}
						a.SendMessage(Message{hdr, MessageContent{"OrderProcessed.."}})
						
		   		    }
					
					  
				   

			   }
		
}
}
