package platform 

import (
    "ties433.it.jyu.fi/group1/agent"	
	"log"
	"fmt"
)



type AgentManager struct{
	
	 deliveryAgents map[int] agent.AgentInterface
     orderAgents    map[int] agent.AgentInterface
	 Send     chan  agent.Message
}

func  NewAgentManager() AgentManager {
	return AgentManager{map[int]agent.AgentInterface{}, map[int]agent.AgentInterface{},make(chan agent.Message)}
}


func (m AgentManager) createOrderAgent(id int) *agent.OrderAgent{
	 receiveChannel := make(chan agent.Message) 
 	 _, found := m.deliveryAgents[id]
	 
	 if (found){
		a := agent.NewOrderAgent(len(m.deliveryAgents)+1 ,receiveChannel)
		return a
	 }else{
 	 	a := agent.NewOrderAgent(id,receiveChannel)
		return a
	 }
	 	    	 
}

func (m AgentManager) createDeliveryAgent(id int) *agent.DeliveryAgent{
	 receiveChannel := make(chan agent.Message) 
	 a := agent.NewDeliveryAgent(id,receiveChannel)
	 return a
	 
 }

func (m AgentManager) AddOrderAgent(a agent.AgentInterface){
	a.SetOutChannel(m.Send)
	m.orderAgents[a.GetId()] = a
}

func (m AgentManager) AddeliveryAgent(a agent.AgentInterface){
	a.SetOutChannel(m.Send)
	m.deliveryAgents[a.GetId()] = a
}

func (m AgentManager) RunOrderAgent(id int) {
	a, found := m.orderAgents[id]

	if found {
		go a.ListenMessage()
	} else {
		log.Fatal("Agent not found :Failed  to run an agnet in the manager")
	}
}

func (m AgentManager) ManagerToAgent(msg agent.Message) {
	a, found := m.deliveryAgents[msg.Header.Target]
	if found {
		a.GetMessage(msg)
	}else{
		a, found := m.orderAgents[msg.Header.Target]
		if found {
			a.GetMessage(msg)
		}else {
		log.Fatal("Agent not found :Failed  to find  reciver agent")
	}
	}	 
}

func (m AgentManager) RunAll() {
	
	for k := range m.deliveryAgents{
		go m.deliveryAgents[k].ListenMessage()
	}
	for i:= range m.orderAgents {
		go m.orderAgents[i].ListenMessage()
	}
}