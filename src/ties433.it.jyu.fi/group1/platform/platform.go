package platform

import (
    "ties433.it.jyu.fi/group1/agent"	
	"fmt"
)

type Platform struct {
	Manager      AgentManager 
	platform_id  int
}

func NewPlatform(id int) Platform {
	return Platform{
		platform_id: id,
		Manager:  NewAgentManager(),
    }
}

func   InitalizePlatform(numdelivery_final int, numorder_inital int,id int) *Platform{
	
	p := NewPlatform(id)
	
	// initialize and start all the agents
	for i := 0; i < numdelivery_final; i++ {
		p.Manager.AddeliveryAgent(p.Manager.createDeliveryAgent(i))//assign unique id 
	}
	
	// initialize and start all the agents
	for i := 0; i < numorder_inital; i++ {
		p.Manager.AddOrderAgent(p.Manager.createOrderAgent(i)) //assign unique id 
	} 
	
	return &p
	
}


func (p Platform) ListenLocalMessages() {
	for {
		msg := <-p.Manager.Send
		go p.HandleMessage(msg)
	}
}

func (p Platform) HandleMessage(msg agent.Message) {
	fmt.Printf("%d=> %d:: \"%s\"\n", msg.Header.Sender, msg.Header.Target, msg.Content.Data)
	if  true{
		p.Manager.ManagerToAgent(msg)
	}
}