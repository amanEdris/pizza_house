package sensor

import (
	///"fmt"
	"math/rand"
	"time"
	"log"
)

type Status string

type TrafficRecord struct {
	TimeStamp time.Time
	Status    Status
}

type TrafficArgs struct {
}

type TrafficSensor struct {
	CurrentRecord TrafficRecord
}

//making some false data based on current time
func (tsensor *TrafficSensor) GetTrafficRecord(args TrafficArgs, CurrentRecord *TrafficRecord) error {
	
	log.Println("Status:", tsensor.CurrentRecord.Status)

	CurrentRecord.Status = tsensor.CurrentRecord.Status
	CurrentRecord.TimeStamp = tsensor.CurrentRecord.TimeStamp
	return nil
}

func (tsensor *TrafficSensor) SetTrafficStatus() {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	randomValue := random.Float32()
	tsensor.CurrentRecord.TimeStamp = time.Now()
	if randomValue < 0.33 {
		tsensor.CurrentRecord.Status = "Low Traffic"
	} else if randomValue < 0.66 {
		tsensor.CurrentRecord.Status = "High Traffic"
	} else {
		tsensor.CurrentRecord.Status = "Normal Traffic"
	}
	log.Printf("Traffic Data set to %++v\n", tsensor)
	return
}

//testing functing 
func CheckTrafficstatus( traffic string) bool{
	
	traffic = "busy"
	
return true
}
